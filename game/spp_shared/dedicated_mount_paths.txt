"ServerPaths"
{
    // Dedicated servers use this file to locate games and mods.
    // The keyname should be the Steam AppID of the game and the value
    // should be the path where that game's content is located.
    // The keyname 'sourcemods' should point to where all the server's 
    // source engine mod content is located.

    "sourcemods"    "C:\Program Files (x86)\Steam\steamapps\sourcemods"
    "220"           "D:\SteamLibrary\steamapps\common\Half-Life 2"
    "240"           "D:\SteamLibrary\steamapps\common\Counter-Strike Source"
    "280"           "D:\SteamLibrary\steamapps\common\Half-Life 2"
    "320"           "D:\SteamLibrary\steamapps\common\Half-Life 2 Deathmatch"
    "340"           "D:\SteamLibrary\steamapps\common\Half-Life 2"
    "360"           "D:\SteamLibrary\steamapps\common\Half-Life 1 Source Deathmatch"
    "380"           "D:\SteamLibrary\steamapps\common\Half-Life 2"
    "420"           "D:\SteamLibrary\steamapps\common\Half-Life 2"
    "400"           "D:\SteamLibrary\steamapps\common\Portal"
    "362890"        "D:\SteamLibrary\steamapps\common\Black Mesa"
	"587650"		"D:\SteamLibrary\steamapps\common\Half-Life 2 Downfall"
	"747250"		"D:\SteamLibrary\steamapps\common\Half-Life 2 Year Long Alarm"
	"235780"		"D:\SteamLibrary\steamapps\common\MINERVA"
	"290930"		"D:\SteamLibrary\steamapps\common\half-life 2 update"
	"317790"		"D:\SteamLibrary\steamapps\common\Rexaura"
	"399120"		"D:\SteamLibrary\steamapps\common\Prospekt"
	"365300"		"D:\SteamLibrary\steamapps\common\Transmissions Element 120"
	"714070"		"D:\SteamLibrary\steamapps\common\Entropy Zero"
}