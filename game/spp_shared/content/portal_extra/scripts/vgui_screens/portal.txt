"VGUI_Screens"
{
	"slideshow_display_screen"
	{
		"type"		"slideshow_display_screen"
		"pixelswide"	256
		"pixelshigh"	128
		"acceptsinput"  0
		"resfile"	"scripts/screens/slideshow_display_screen.res"
	}

	"neurotoxin_countdown_screen"
	{
		"type"		"neurotoxin_countdown_screen"
		"pixelswide"	256
		"pixelshigh"	128
		"acceptsinput"  0
		"resfile"	"scripts/screens/neurotoxin_countdown_screen.res"
	}

	"portal_stats_display_screen"
	{
		"type"		"portal_stats_display_screen"
		"pixelswide"	512
		"pixelshigh"	256
		"acceptsinput"  0
		"resfile"	"scripts/screens/portal_stats_display_screen.res"
	}
}
