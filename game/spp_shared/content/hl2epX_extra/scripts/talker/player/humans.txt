
#include "talker/npc_citizen.txt"

// ============================================
//	AUTOMATIC RESPONSES
// ============================================

response PlayerCitExpressionIdle
{
	scene		"scenes/Expressions/citizen_normal_alert_01.vcd"
}

response PlayerCitExpressionCombat
{
	scene		"scenes/Expressions/citizen_angry_combat_01.vcd"
}

rule PlayerCitExpIdle
{
	criteria	IsPlayer IsPlayerVoiceCitizen ConceptPlayerExpression PlayerNotHasEnemy
	response	PlayerCitExpressionIdle
}

rule PlayerCitExpCombat
{
	criteria	IsPlayer IsPlayerVoiceCitizen ConceptPlayerExpression PlayerHasEnemy
	response	PlayerCitExpressionCombat
}

// using CitizenHello
rule PlayerHello
{
	criteria	ConceptPlayerHello IsPlayer IsPlayerVoiceCitizen
	response	CitizenHello
}

// TLK_PLAYER_BEHIND - speak this when there is an enemy behind a player you are looking at

response PlayerResponseBehindYou
{
	scene		"scenes/npc/$gender01/behindyou01.vcd"
	scene		"scenes/npc/$gender01/behindyou01.vcd"		weight 2
}

rule PlayerBehindYou
{
	criteria	ConceptPlayerBehind IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseBehindYou
}

// TLK_PLAYER_RELOAD - speak this when player reloads and there are teammates nearby

response PlayerResponseReloading
{
	scene		"scenes/npc/$gender01/coverwhilereload01.vcd"	weight 2
	scene		"scenes/npc/$gender01/coverwhilereload02.vcd"
}

rule PlayerReloading
{
	criteria	ConceptPlayerCoverReload IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseReloading
}

// TLK_PLAYER_PAIN

response PlayerDiedDefault
{
	speak		"Player.Death" noscene
}

rule PlayerDied
{
	criteria	ConceptPlayerDied IsPlayer IsPlayerVoiceDefault
	response	PlayerDiedDefault
}

response CitizenPlayerResponsePain
{
	// TODO: look at this
	speak		"NPC_Citizen.Pain01"
	speak		"NPC_Citizen.Pain02"
	speak		"NPC_Citizen.Pain03"
	speak		"NPC_Citizen.Pain04"
	speak		"NPC_Citizen.Pain05"
	speak		"NPC_Citizen.Pain06"
//Trav|Edt - add more Pain responces
	speak		"NPC_Citizen.Pain07"
	speak		"NPC_Citizen.Pain08"
	speak		"NPC_Citizen.Pain09"
	speak		"NPC_Citizen.ow01"
	speak		"NPC_Citizen.ow02"
	// speak		"NPC_Citizen.myarm01"
	// speak		"NPC_Citizen.myarm02"
	// speak		"NPC_Citizen.mygut02"
	// speak		"NPC_Citizen.myleg01"
	// speak		"NPC_Citizen.myleg02"
}

rule CitizenPlayerDie
{
	criteria	ConceptPlayerDied IsPlayer IsPlayerVoiceCitizen
	response	CitizenPlayerResponsePain
}

rule CitizenPlayerPain
{
	criteria	ConceptPlayerPain IsPlayer IsPlayerVoiceCitizen
	response	CitizenPlayerResponsePain
}

rule PlayerShotArm
{
   criteria    ConceptPlayerPain IsPlayer IsPlayerVoiceCitizen ShotInArm
   response    CitizenShotArm
}

rule PlayerShotLeg
{
   criteria    ConceptPlayerPain IsPlayer IsPlayerVoiceCitizen ShotInLeg
   response    CitizenShotLeg
}

rule PlayerShotGut
{
   criteria    ConceptPlayerPain IsPlayer IsPlayerVoiceCitizen ShotInGut
   response    CitizenShotGut
}

// TLK_PLAYER_GIVEAMMO

response PlayerResponseGiveAmmo
{
//	scene "scenes/npc/$gender01/ammo01.vcd" delay 0
//	scene "scenes/npc/$gender01/ammo02.vcd" delay 0
	scene "scenes/npc/$gender01/ammo03.vcd" delay 0
	scene "scenes/npc/$gender01/ammo04.vcd" delay 0
	scene "scenes/npc/$gender01/ammo05.vcd" delay 0
}

rule PlayerGiveAmmo
{
	criteria	ConceptPlayerGiveAmmo IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseGiveAmmo
}

// TLK_PLAYER_GIVEHEALTH

response PlayerResponseGiveHealth
{
	scene "scenes/npc/$gender01/health01.vcd" delay 0
	scene "scenes/npc/$gender01/health02.vcd" delay 0
	scene "scenes/npc/$gender01/health03.vcd" delay 0
	scene "scenes/npc/$gender01/health04.vcd" delay 0
	scene "scenes/npc/$gender01/health05.vcd" delay 0
}

rule PlayerGiveHealth
{
	criteria	ConceptPlayerGiveHealth IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseGiveHealth
}

// TLK_PLAYER_TAUNT

rule PlayerTaunt
{
	criteria	ConceptPlayerKill IsPlayer IsPlayerVoiceCitizen
	response	CitizenEnemyDead
}

response "PlayerKillStrider"
{
	scene "scenes/outland_12/citizen_celebrate2.vcd"
	scene "scenes/outland_12/citizen_celebrate4.vcd"
	scene "scenes/outland_12/citizen_celebrate5.vcd"
	scene "scenes/outland_12/citizen_celebrate7.vcd"
}

rule PlayerKillStrider
{ 
	criteria		ConceptPlayerKill IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsStrider IsPlayerModelMale
	response		PlayerKillStrider
}

// TLK_PLAYER_DEAD
// TLK_PLAYER_DEAD_ANSWER

response PlayerResponseDeadQuestion
{
	scene	"scenes/npc/$gender01/gordead_ques01.vcd"
	scene	"scenes/npc/$gender01/gordead_ques02.vcd"
	scene	"scenes/npc/$gender01/gordead_ques06.vcd"
	scene	"scenes/npc/$gender01/gordead_ques07.vcd"
	scene	"scenes/npc/$gender01/gordead_ques10.vcd"
	scene	"scenes/npc/$gender01/gordead_ques11.vcd"
//Trav|Edt - Add more DeadQuestions
	scene	"scenes/npc/$gender01/question05.vcd"
	scene	"scenes/npc/$gender01/question06.vcd"
	scene	"scenes/npc/$gender01/question10.vcd"
	scene	"scenes/npc/$gender01/question11.vcd"
	scene	"scenes/npc/$gender01/question12.vcd"
	scene	"scenes/npc/$gender01/question14.vcd"
	scene	"scenes/npc/$gender01/question16.vcd"
	scene	"scenes/npc/$gender01/question17.vcd"
	scene	"scenes/npc/$gender01/question18.vcd"
	scene	"scenes/npc/$gender01/question20.vcd"
	scene	"scenes/npc/$gender01/question21.vcd"
	scene	"scenes/npc/$gender01/question25.vcd"
	scene	"scenes/npc/$gender01/question26.vcd"
	scene	"scenes/npc/$gender01/question30.vcd"
	scene	"scenes/npc/$gender01/no01.vcd"
	scene	"scenes/npc/$gender01/no02.vcd"
	scene	"scenes/npc/$gender01/ohno.vcd"
//EP1 scenes (players w/o eps mounted will hear nothing, so keep episodic stuff to autoresponces only)
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled02.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled03.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled04.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled07.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled08.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled11.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled12.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_buddykilled13.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_evac_casualty02.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_evac_casualty03.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_evac_casualty05.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_evac_casualty09.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_evac_defendus06.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_notice_gravgunkill03.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_remarks04.vcd"
	scene	"scenes/episode_1/npc/$gender01/cit_remarks18.vcd"

}

response PlayerResponseDeadAnswer
{
	scene	"scenes/npc/$gender01/gordead_ans02.vcd"
	scene	"scenes/npc/$gender01/gordead_ans03.vcd"
	scene	"scenes/npc/$gender01/gordead_ans04.vcd"
	scene	"scenes/npc/$gender01/gordead_ans05.vcd"
	scene	"scenes/npc/$gender01/gordead_ans06.vcd"
	scene	"scenes/npc/$gender01/gordead_ans07.vcd"
	scene	"scenes/npc/$gender01/gordead_ans08.vcd"
	scene	"scenes/npc/$gender01/gordead_ans09.vcd"
	scene	"scenes/npc/$gender01/gordead_ans10.vcd"
	scene	"scenes/npc/$gender01/gordead_ans14.vcd"
	scene	"scenes/npc/$gender01/gordead_ans15.vcd"
	scene	"scenes/npc/$gender01/gordead_ans18.vcd"
	scene	"scenes/npc/$gender01/gordead_ans19.vcd"
	scene	"scenes/npc/$gender01/gordead_ans20.vcd"
//Trav|Edt - Add Scenes
	scene	"scenes/npc/$gender01/gordead_ans01.vcd"
	scene	"scenes/npc/$gender01/gordead_ans11.vcd"
	scene	"scenes/npc/$gender01/gordead_ans12.vcd"
	scene	"scenes/npc/$gender01/gordead_ans13.vcd"
	scene	"scenes/npc/$gender01/gordead_ans16.vcd"
	scene	"scenes/npc/$gender01/gordead_ans17.vcd"
	scene	"scenes/npc/$gender01/answer03.vcd"
	scene	"scenes/npc/$gender01/answer04.vcd"
	scene	"scenes/npc/$gender01/answer11.vcd"
	scene	"scenes/npc/$gender01/answer16.vcd"
	scene	"scenes/npc/$gender01/answer18.vcd"
	scene	"scenes/npc/$gender01/answer25.vcd"
	scene	"scenes/npc/$gender01/answer28.vcd"
	scene	"scenes/npc/$gender01/answer36.vcd"
	scene	"scenes/npc/$gender01/answer37.vcd"
	scene	"scenes/npc/$gender01/answer40.vcd"
}

rule PlayerDeadPlayerQuestion
{
	criteria	ConceptPlayerDeadPlayer IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseDeadQuestion
}

rule PlayerDeadPlayerAnswer
{
	criteria	ConceptPlayerDeadPlayerAnswer IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseDeadAnswer
}

// ============================================
//	MANUAL RESPONSES
// ============================================

// TLK_PLAYER_HELP

response CitizenPlayerResponseHelp
{
	scene	"scenes/player/$gender01/help01.vcd"
	scene	"scenes/player/$gender01/help02.vcd"
	scene	"scenes/player/$gender01/help03.vcd"
}

rule PlayerHelp
{
	criteria	ConceptPlayerHelp IsPlayer IsPlayerVoiceCitizen
	response	CitizenPlayerResponseHelp
}

// TLK_PLAYER_THANKS

response PlayerResponseThanks
{
	// TODO: stick some scenes in here
}

rule PlayerThanks
{
	criteria	ConceptPlayerThanks IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseThanks
}

rule PlayerCitizenAntLionDanger
{
	criteria		IsPlayer IsPlayerVoiceCitizen ConceptPlayerAttacking PlayerEnemyIsAntLion
	response		CitizenAntLionDanger
}

rule PlayerCitizenFiringRPG
{
	criteria		IsPlayer IsPlayerVoiceCitizen IsRPGUser ConceptPlayerAttacking
	response		CitizenFiringRPG
}

// TLK_PLAYER_INCOMING

response PlayerResponseIncoming
{
	scene		"scenes/npc/$gender01/watchout.vcd"
	scene		"scenes/npc/$gender01/headsup02.vcd"
	scene		"scenes/npc/$gender01/incoming02.vcd"
	scene		"scenes/npc/$gender01/headsup01.vcd"
//Trav|Edt - Add Scenes
	scene	"scenes/npc/$gender01/uhoh.vcd"
	scene	"scenes/npc/$gender01/strider_run.vcd"
	scene	"scenes/npc/$gender01/behindyou01.vcd"
	scene	"scenes/npc/$gender01/behindyou02.vcd"
	scene	"scenes/npc/$gender01/getdown02.vcd"
	scene	"scenes/npc/$gender01/gethellout.vcd"
	scene	"scenes/npc/$gender01/heretheycome01.vcd"
	scene	"scenes/npc/$gender01/takecover02.vcd"
}

rule PlayerIncoming
{
	criteria	ConceptPlayerIncoming IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseIncoming
}

// TLK_PLAYER_ENEMY

rule PlayerEnemy
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen

//	response	PlayerResponseEnemy
	response	PlayerResponseIncoming	// TODO: find sounds that are more like "I see an enemy"
}

// TODO: at some point, move away from using these npc_citizen responses

// using CitizenCombineGroup
rule PlayerEnemyCombine
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsCombine
	response	CitizenCombineGroup
}

// using CitizenHeadcrabGroup
rule PlayerEnemyHeadcrab
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsHeadcrab
	response	CitizenHeadcrabGroup
}

rule PlayerEnemyHeadcrabBlack
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsHeadcrabBlack
	response	CitizenHeadcrabGroup
}

rule PlayerEnemyHeadcrabPoison
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsHeadcrabPoison
	response	CitizenHeadcrabGroup
}

rule PlayerEnemyHeadcrabFast
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsHeadcrabFast
	response	CitizenHeadcrabGroup
}

rule PlayerEnemyZombie
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsZombie
	response	CitizenZombieGroup
}

rule PlayerEnemyZombieTorso
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsZombieTorso
	response	CitizenZombieGroup
}

rule PlayerEnemyZombieBlack
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsZombieBlack
	response	CitizenZombieGroup
}

rule PlayerEnemyZombieFast
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsZombieFast
	response	CitizenZombieGroup
}

rule PlayerEnemyScanner
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsScanner
	response	CitizenScannersGroup
}

response PlayerEnemyStriderGroup
{
	scene "scenes/npc/$gender01/strider.vcd"
}

rule PlayerEnemyStrider
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsStrider
	response	PlayerEnemyStriderGroup
}

rule PlayerEnemyManhack
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsManhack
	response	CitizenManhacksGroup
}

rule PlayerEnemyMetro
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsMetro
	response	CitizenMetroGroup
}

rule PlayerEnemyZombine
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsZombine
	response	CitizenZombine
}

rule PlayerEnemyHunter
{
	criteria	ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen PlayerEnemyIsHunter
	response	CitizenCombineGroup
}

rule PlayerCitizenStartCombatTurretFloor
{
	criteria    ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen
	playerenemy npc_turret_floor required
	response    CitizenTurret
}

rule PlayerCitizenStartCombatTurretGround
{
	criteria    ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen
	playerenemy npc_turret_gound required
	response    CitizenTurretGround
}

rule PlayerCitizenStartCombatTurretCeiling
{
	criteria    ConceptPlayerEnemy IsPlayer IsPlayerVoiceCitizen
	playerenemy npc_turret_ceiling required
	response    CitizenTurret
}

// TLK_PLAYER_FOLLOW

response PlayerResponseFollow
{
	scene		"scenes/npc/$gender01/squad_away03.vcd"		weight 4
	scene		"scenes/npc/$gender01/squad_follow02.vcd"
	scene		"scenes/npc/$gender01/squad_follow03.vcd"
	scene		"scenes/npc/$gender01/squad_away01.vcd"		weight 3
	scene		"scenes/npc/$gender01/squad_away02.vcd"
//Trav|Edt - add more Follow scenes
	scene	"scenes/npc/$gender01/overhere01.vcd"
}

rule PlayerFollow
{
	criteria	ConceptPlayerFollow IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseFollow
}

// TLK_PLAYER_LEAD

response PlayerResponseLeadOn
{
	scene	"scenes/npc/$gender01/leadtheway01.vcd"
	scene	"scenes/npc/$gender01/leadtheway02.vcd"
}

rule PlayerLeadOn
{
	criteria	ConceptPlayerLead IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseLeadOn
}

// TLK_PLAYER_GO

response PlayerResponseLetsGo
{
	scene	"scenes/npc/$gender01/letsgo01.vcd"
	scene	"scenes/npc/$gender01/letsgo02.vcd"
//Trav|Edt - add more LetsGo scenes
	scene	"scenes/npc/$gender01/doingsomething.vcd"
	scene	"scenes/npc/$gender01/getgoingsoon.vcd"
	scene	"scenes/npc/$gender01/waitingsomebody.vcd"
}

rule PlayerLetsGo
{
	criteria	ConceptPlayerGo IsPlayer IsPlayerVoiceCitizen
	response	PlayerResponseLetsGo
}

rule PlayerLetsGo
{
	criteria	ConceptPlayerAnswer IsPlayer IsPlayerVoiceCitizen
	response	CitizenAnswer
}


