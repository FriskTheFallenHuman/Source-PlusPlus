
"background.interiorA"
{

	"fadetime" "4"

	"playlooping"
	{
		"volume"	"1"
		"pitch"		"100"
		"wave"		"scapes/3D/pipe_burst_loop.wav"
		"origin"	"-6302.031250, -5218.000000, 1006.937500;"
		"soundlevel" 	"SNDLVL_75dB"
	}


	"playlooping"
	{
		"volume"	"1"
		"pitch"		"100"
		"wave"		"scapes/3D/water_drip_loop.wav"
		"origin"	"-6722.187500, -6547.187500, 945.000000;"
		"soundlevel" 	"SNDLVL_80dB"
	}

	"playlooping"
	{
		"volume"	"1"
		"pitch"		"120"
		"wave"		"scapes/3D/water_drip_loop.wav"
		"origin"	"-6365.218750, -6011.593750, 934.656250;"
		"soundlevel" 	"SNDLVL_80dB"
	}

	"playlooping"
	{
		"volume"	".71"
		"pitch"		"70"
		"wave"		"scapes/3D/steam_loop_01.wav"
		"origin"	"-7184.625000, -5127.875000, 865.531250;"
		"soundlevel" 	"SNDLVL_75dB"
	}

	"playlooping"
	{
		"volume"	".79"
		"pitch"		"100"
		"wave"		"scapes/3D/steam_loop_01.wav"
		"origin"	"-6608.625000, -5681.531250, 957.937500;"
		"soundlevel" 	"SNDLVL_70dB"
	}



	"playsoundscape"
	{
		"name" "interior2.MedB"
		"volume" "1"
	}

	"playrandom"
	{
		"time"		"10, 20"
		"volume"	"0.6, 0.9"
		"pitch"		"80, 110"
		"soundlevel"	"0"
		"rndwave"
		{
			"wave"	"#scapes/2D/exterior1/disquiet01.wav"
			"wave"	"#scapes/2D/exterior1/disquiet02.wav"
			"wave"	"#scapes/2D/exterior1/disquiet03.wav"
		}
	}

}